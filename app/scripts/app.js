'use strict';

/**
 * @ngdoc overview
 * @name glassCompanyApp
 * @description
 * # glassCompanyApp
 *
 * Main module of the application.
 */
angular
  .module('glassCompanyApp', [
    'ngAnimate',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/results/:details', {
        templateUrl: 'views/results.html',
        controller: 'ResultsCtrl'
      })
      .when('/results/details/:details', {
        templateUrl: 'views/details.html',
        controller: 'DetailsCtrl'
      });
      //.otherwise({
      //  redirectTo: '/'
      //});
  });
