'use strict';

/**
 * @ngdoc service
 * @name glassCompanyApp.gcCache
 * @description
 * # gcCache
 * Service in the glassCompanyApp.
 */
angular.module('glassCompanyApp')
  .service('gcCache',[ '$cacheFactory', function ($cacheFactory) {
    return $cacheFactory('gcCache');
  }]);
