'use strict';

/**
 * @ngdoc service
 * @name glassCompanyApp.companyService
 * @description
 * # companyService
 * Service in the glassCompanyApp.
 */
angular.module('glassCompanyApp')
  .service('companyService',['$http', '$q', 'gcCache', function ($http, $q, gcCache) {

    var search = function( options ){

      var company = options.details;
      //TODO: put key in server side or in enironment variable
      var searchUrl = 'http://api.glassdoor.com/api/api.htm?t.p=27901&t.k=bOwOg720cyq&userip=0.0.0.0&useragent&format=json&v=1&action=employers&q='+ company +'&callback=JSON_CALLBACK';
      var deferred = $q.defer();

      $http({method: 'JSONP', url: searchUrl})
        .success(function(data) {
          gcCache.put('results', data.response);
          deferred.resolve(data.response);
        }).
        error(function(error) {
          console.log(error);
          deferred.reject();
        });

      return deferred.promise;
    };

    return {
      search: search
    };

  }]);
