'use strict';

/**
 * @ngdoc function
 * @name glassCompanyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the glassCompanyApp
 */
angular.module('glassCompanyApp')
  .controller('MainCtrl', ['$scope', 'gcCache', '$location', function ($scope, gcCache, $location) {

    $scope.search = function(company) {
      //Remove cache
      gcCache.remove('results');

      //Change route to results page
      $location.path('results/' + company);
    };

  }]);
