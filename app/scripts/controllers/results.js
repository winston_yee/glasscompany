'use strict';

/**
 * @ngdoc function
 * @name glassCompanyApp.controller:ResultsCtrl
 * @description
 * # ResultsCtrl
 * Controller of the glassCompanyApp
 */
angular.module('glassCompanyApp')
  .controller('ResultsCtrl', ['$scope', '$routeParams', '$location', 'companyService', 'gcCache', function ($scope, $routeParams, $location, companyService, gcCache) {

    var company = '';
    var cacheResults = null;

    $scope.init = function() {
      company = $routeParams.details;
      cacheResults = gcCache.get('results');

      //Check cache
      if (cacheResults) {
        $scope.results = cacheResults;
      }else{
        $scope.searchCompany(company);
      }
    };

    //Search and display results
    $scope.searchCompany = function(){
      var options = {details: company};

      companyService.search(options).then(
        function(data) {
          $scope.results = data;
        }
      );
    };

    //Change routes, can move to a util service
    $scope.goTo = function ( path ) {
      $location.path( path );
    };
  }]);
