'use strict';

/**
 * @ngdoc function
 * @name glassCompanyApp.controller:DetailsCtrl
 * @description
 * # DetailsCtrl
 * Controller of the glassCompanyApp
 */
angular.module('glassCompanyApp')
  .controller('DetailsCtrl',['$scope','$routeParams', 'companyService', 'gcCache', function ($scope, $routParams, companyService, gcCache) {
    var cacheResults = gcCache.get('results');
    var company = $routParams.details;

    //TODO: Put in reusable service, duplicate code like results.js
    //Search and display results
    var searchCompany = function(){
      var options = {details: company};

      companyService.search(options).then(
        function(data) {
          $scope.details = data.employers[0];
        }
      );
    };

    //TODO: Put in reusable service, duplicate code like results.js
    //Check cache
    if (cacheResults) {
      //Look through results array and return first object
      $scope.details = cacheResults.employers.filter(function(obj){
          return obj.name === company;
        })[0];
    }else{
      searchCompany(company);
    }
  }]);
