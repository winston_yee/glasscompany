'use strict';

xdescribe('Service: companyService', function () {

  // load the service's module
  beforeEach(module('glassCompanyApp'));

  // instantiate service
  var companyService;
  beforeEach(inject(function (_companyService_) {
    companyService = _companyService_;
  }));

  it('should do something', function () {
    expect(!!companyService).toBe(true);
  });

});
