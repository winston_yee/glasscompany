'use strict';

xdescribe('Service: gcCache', function () {

  // load the service's module
  beforeEach(module('glassCompanyApp'));

  // instantiate service
  var gcCache;
  beforeEach(inject(function (_gcCache_) {
    gcCache = _gcCache_;
  }));

  it('should do something', function () {
    expect(!!gcCache).toBe(true);
  });

});
